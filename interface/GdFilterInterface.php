<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;
use Samy\Validation\ValidationException;

/**
 * Describes GD Filter interface.
 */
interface GdFilterInterface
{
    /**
     * Reverses all colors of the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function reverseColor(array $Selection): self;

    /**
     * Converts the image into grayscale.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function grayscale(array $Selection): self;

    /**
     * Uses edge detection to highlight the edges in the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function edgeDetect(array $Selection): self;

    /**
     * Embosses the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function embosses(array $Selection): self;

    /**
     * Blurs the image.
     *
     * @param array<string,int> $Blur The blur filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function blur(array $Blur): self;

    /**
     * Uses mean removal to achieve a "sketchy" effect.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function sketchy(array $Selection): self;

    /**
     * Changes the brightness of the image.
     * Level: -255 ~ 255
     *
     * @param array<string,int> $Brightness The brightness filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function brightness(array $Brightness): self;

    /**
     * Changes the contrast of the image.
     * Level: -100 ~ 100
     *
     * @param array<string,int> $Contrast The contrast filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function contrast(array $Contrast): self;

    /**
     * Makes the image smoother.
     * Level: -8 ~ 8
     *
     * @param array<string,int> $Smooth The smooth filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function smooth(array $Smooth): self;

    /**
     * Applies pixelation effect to the image.
     *
     * @param array<string,mixed> $Pixelate The pixelate filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function pixelate(array $Pixelate): self;

    /**
     * Applies scatter effect to the image.
     *
     * @param array<string,mixed> $Scatter The scatter filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function scatter(array $Scatter): self;

    /**
     * Converts the image into specify color.
     *
     * @param array<string,mixed> $Colorize The colorize filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function colorize(array $Colorize): self;
}
