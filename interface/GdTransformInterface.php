<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;
use Samy\Validation\ValidationException;

/**
 * Describes GD Transform interface.
 */
interface GdTransformInterface
{
    /**
     * Flip image with a given mode.
     *
     * @param int $Mode The flip mode.
     * @throws GdException If error.
     * @return static
     */
    public function flip(int $Mode): self;

    /**
     * Rotate image with a given angle.
     *
     * @param float $Angle The rotate angle.
     * @throws GdException If error.
     * @return static
     */
    public function rotate(float $Angle): self;

    /**
     * Copy source image.
     *
     * @param array<string,mixed> $Copy The copy transform.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function copy(array $Copy): self;

    /**
     * Crop image.
     *
     * @param array<string,int> $Selection The transform selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function crop(array $Selection): self;

    /**
     * Resize stretch image.
     *
     * @param int $Width The resize width.
     * @param int $Height The resize height.
     * @throws GdException If error.
     * @return static
     */
    public function resize(int $Width, int $Height): self;

    /**
     * Fill thumbnail image.
     *
     * @param int $Width The thumbnail width.
     * @param int $Height The thumbnail height.
     * @throws GdException If error.
     * @return static
     */
    public function thumbnail(int $Width, int $Height): self;

    /**
     * Transform corner.
     *
     * @param array<string,int> $Corner The corner transform.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function corner(array $Corner): self;
}
