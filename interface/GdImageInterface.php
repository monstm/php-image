<?php

namespace Samy\Image\Interface;

use GdImage;
use Samy\Image\GdException;

/**
 * Describes GD Image interface.
 */
interface GdImageInterface
{
    /**
     * Check if instance has a resource image.
     *
     * @return bool
     */
    public function hasImage(): bool;

    /**
     * Retrieve resource image.
     *
     * @throws GdException If error.
     * @return GdImage
     */
    public function getImage(): GdImage;

    /**
     * Separates any underlying resource from the instance.
     * After the instance has been detached, the instance is in an unusable state.
     *
     * @throws GdException If error.
     * @return GdImage
     */
    public function detach(): GdImage;

    /**
     * Retrieve image type.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getType(): int;

    /**
     * Return an instance with the provided image type.
     *
     * @param int $Type The image type.
     * @throws GdException If error.
     * @return static
     */
    public function withType(int $Type): self;

    /**
     * Retrieve image width.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getWidth(): int;

    /**
     * Retrieve image height.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getHeight(): int;

    /**
     * Retrieve image mime content-type.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getMimeType(): string;

    /**
     * Retrieve image file extension.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getExtension(): string;

    /**
     * Retrieve image file content.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getContent(): string;

    /**
     * Retrieve image file size.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getSize(): int;

    /**
     * Retrieve image md5 hash.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getMd5(): string;

    /**
     * Retrieve image sha1 hash.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getSha1(): string;

    /**
     * Save image file.
     *
     * @param string $Filename The filename.
     * @throws GdException If error.
     * @return static
     */
    public function save(string $Filename): self;
}
