<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;

/**
 * Describes GD Pixel interface.
 */
interface GdPixelInterface
{
    /**
     * Retrieve argb color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getArgb(int $X, int $Y): int;

    /**
     * Return an instance with provided argb color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The argb value
     * @throws GdException If error.
     * @return static
     */
    public function withArgb(int $X, int $Y, int $Value): self;

    /**
     * Retrieve alpha color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getAlpha(int $X, int $Y): int;

    /**
     * Return an instance with provided alpha color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The alpha value
     * @throws GdException If error.
     * @return static
     */
    public function withAlpha(int $X, int $Y, int $Value): self;

    /**
     * Retrieve red color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getRed(int $X, int $Y): int;

    /**
     * Return an instance with provided red color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The red value
     * @throws GdException If error.
     * @return static
     */
    public function withRed(int $X, int $Y, int $Value): self;

    /**
     * Retrieve green color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getGreen(int $X, int $Y): int;

    /**
     * Return an instance with provided green color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The green value
     * @throws GdException If error.
     * @return static
     */
    public function withGreen(int $X, int $Y, int $Value): self;

    /**
     * Retrieve blue color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getBlue(int $X, int $Y): int;

    /**
     * Return an instance with provided blue color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The blue value
     * @throws GdException If error.
     * @return static
     */
    public function withBlue(int $X, int $Y, int $Value): self;
}
