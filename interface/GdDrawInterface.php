<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;
use Samy\Validation\ValidationException;

/**
 * Describes GD Draw interface.
 */
interface GdDrawInterface
{
    /**
     * Draw a line.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawLine(array $Area): self;

    /**
     * Draw a rectangle.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawRectangle(array $Area): self;

    /**
     * Draw a filled rectangle.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledRectangle(array $Area): self;

    /**
     * Draw an ellipse.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawEllipse(array $Area): self;

    /**
     * Draw a filled ellipse.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledEllipse(array $Area): self;

    /**
     * Draw an arc.
     *
     * @param array<string,int> $Arc The draw arc.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawArc(array $Arc): self;

    /**
     * Draw a filled arc.
     *
     * @param array<string,int> $Arc The draw arc.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledArc(array $Arc): self;

    /**
     * Draw a polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawPolygon(array $Polygon): self;

    /**
     * Draw a filled polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledPolygon(array $Polygon): self;

    /**
     * Draw an open polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawOpenPolygon(array $Polygon): self;
}
