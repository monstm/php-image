<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;

/**
 * Describes GD Property interface.
 */
interface GdPropertyInterface
{
    /**
     * Retrieve image blend mode.
     *
     * @throws GdException If error.
     * @return bool
     */
    public function getBlendMode(): bool;

    /**
     * Return an instance with provided image blend mode.
     *
     * @param bool $BlendMode The blend mode.
     * @throws GdException If error.
     * @return static
     */
    public function withBlendMode(bool $BlendMode): self;

    /**
     * Retrieve the thickness of line draw.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getThickness(): int;

    /**
     * Return an instance with provided thickness of line draw.
     *
     * @param int $Thickness The thickness.
     * @throws GdException If error.
     * @return static
     */
    public function withThickness(int $Thickness): self;

    /**
     * Retrieve the style of line draw.
     *
     * @throws GdException If error.
     * @return array<int>
     */
    public function getStyle(): array;

    /**
     * Return an instance with provided style of line draw.
     *
     * @param array<int> $Style The style.
     * @throws GdException If error.
     * @return static
     */
    public function withStyle(array $Style): self;

    /**
     * Check if instance has a brush image.
     *
     * @return bool
     */
    public function hasBrush(): bool;

    /**
     * Return an instance with provided brush image.
     *
     * @param string $Location The brush location.
     * @throws GdException If error.
     * @return static
     */
    public function withBrush(string $Location): self;
}
