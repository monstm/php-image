<?php

namespace Samy\Image\Interface;

use Samy\Image\GdException;
use Samy\Validation\ValidationException;

/**
 * Describes GD TrueType Fonts interface.
 */
interface GdTtfInterface
{
    /**
     * Retrieve the bounding box of a text using TrueType fonts.
     *
     * @param array<string,mixed> $Ttf The ttf configuration.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return array<string,int>
     */
    public function ttfBox(array $Ttf): array;

    /**
     * Return an instance with the provided TrueType fonts text.
     *
     * @param array<string,mixed> $Ttf The ttf configuration.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function ttfText(array $Ttf): self;
}
