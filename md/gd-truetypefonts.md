# TrueType Fonts

---

## TrueType Fonts Interface

Describes TrueType interface.

### ttfBox

Retrieve the bounding box of a text using TrueType fonts.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$ttf = [
    "size" => 10,
    "angle" => 45,
    "font" => "arial.ttf",
    "text" => "the quick brown fox jumps over the lazy dog",
    "option" => [
        "linespacing" => 2.5
    ]
];

$box = $gd->ttfBox($ttf);
```

| box            | Description                    |
| :------------- | :----------------------------- |
| x1             | lowest X position              |
| y1             | lowest Y position              |
| x2             | highest X position             |
| y2             | highest Y position             |
| width          | width of the box               |
| height         | height of the box              |
| x-center       | center X position              |
| y-center       | center Y position              |
| x-bottom-left  | lower left corner, X position  |
| y-bottom-left  | lower left corner, Y position  |
| x-bottom-right | lower right corner, X position |
| y-bottom-right | lower right corner, Y position |
| x-top-right    | upper right corner, X position |
| y-top-right    | upper right corner, Y position |
| x-top-left     | upper left corner, X position  |
| y-top-left     | upper left corner, Y position  |

### ttfText

Return an instance with the provided TrueType fonts text.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$ttf = [
    "x" => 45,
    "y" => 60,
    "size" => 10,
    "angle" => 45,
    "font" => "arial.ttf",
    "text" => "the quick brown fox jumps over the lazy dog",
    "color" => 0x336699CC,
    "option" => [
        "linespacing" => 2.5
    ]
];

$gd = $gd->ttfText($ttf);
```
