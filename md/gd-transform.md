# Transform

---

## Transform Interface

Describes Transform interface.

### flip

Flip image with a given mode.
Throw GdException if error.

```php
$gd = $gd->flip($mode);
```

### rotate

Rotate image with a given angle.
Throw GdException if error.

```php
$gd = $gd->rotate($angle);
```

### copy

Copy source image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$copy = [
    "location" => "source.png",
    "source-x" => 10,
    "source-y" => 20,
    "source-width" => 40,
    "source-height" => 80,
    "destination-x" => 160,
    "destination-y" => 240,
    "destination-width" => 360,
    "destination-height" => 480
];

$gd = $gd->copy($copy);
```

### crop

Crop image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 10,
    "y" => 20,
    "width" => 40,
    "height" => 80
];

$gd = $gd->crop($selection);
```

### resize

Resize stretch image.
Throw GdException if error.

```php
$gd = $gd->resize($width, $height);
```

### thumbnail

Fill thumbnail image.
Throw GdException if error.

```php
$gd = $gd->thumbnail($width, $height);
```

### corner

Transform corner.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$corner = [
    "radius" => 8,
    "side" => GdCornerSide::ALL,
    "style" => GdCornerStyle::ROUND
];

$gd = $gd->corner($corner);
```
