# Draw

---

## Draw Interface

Describes Draw interface.

### drawLine

Draw a line.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$area = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC
];

$gd = $gd->drawLine($area);
```

### drawRectangle

Draw a rectangle.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$area = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC
];

$gd = $gd->drawRectangle($area);
```

### drawFilledRectangle

Draw a filled rectangle.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$area = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC
];

$gd = $gd->drawFilledRectangle($area);
```

### drawEllipse

Draw an ellipse.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$area = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC
];

$gd = $gd->drawEllipse($area);
```

### drawFilledEllipse

Draw a filled ellipse.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$area = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC
];

$gd = $gd->drawFilledEllipse($area);
```

### drawArc

Draw an arc.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$arc = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC,
    "start-angle" => 40,
    "end-angle" => 320
];

$gd = $gd->drawArc($arc);
```

### drawFilledArc

Draw a filled arc.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$arc = [
    "x1" => 45,
    "y1" => 60,
    "x2" => 150,
    "y2" => 100,
    "color" => 0x336699CC,
    "start-angle" => 40,
    "end-angle" => 320,
    "style" => GdArcStyle::PIE
];

$gd = $gd->drawFilledArc($arc);
```

### drawPolygon

Draw a polygon.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$polygon = [
    "color" => 0x336699CC,
    "points" => [
        ["x" => 10, "y" => 15],
        ["x" => 20, "y" => 25],
        ["x" => 30, "y" => 35],
        ["x" => 40, "y" => 45],
        ["x" => 50, "y" => 55]
    ]
];

$gd = $gd->drawPolygon($polygon);
```

### drawFilledPolygon

Draw a filled polygon.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$polygon = [
    "color" => 0x336699CC,
    "points" => [
        ["x" => 10, "y" => 15],
        ["x" => 20, "y" => 25],
        ["x" => 30, "y" => 35],
        ["x" => 40, "y" => 45],
        ["x" => 50, "y" => 55]
    ]
];

$gd = $gd->drawFilledPolygon($polygon);
```

### drawOpenPolygon

Draw an open polygon.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$polygon = [
    "color" => 0x336699CC,
    "points" => [
        ["x" => 10, "y" => 15],
        ["x" => 20, "y" => 25],
        ["x" => 30, "y" => 35],
        ["x" => 40, "y" => 45],
        ["x" => 50, "y" => 55]
    ]
];

$gd = $gd->drawOpenPolygon($polygon);
```
