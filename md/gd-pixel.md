# Pixel

---

## Pixel Interface

Describes Pixel interface.

### getArgb

Retrieve argb color of a pixel.
Throw GdException if error.

```php
$argb = $gd->getArgb($x, $y);
```

### withArgb

Return an instance with provided argb color of a pixel.
Throw GdException if error.

```php
$gd = $gd->withArgb($x, $y, $value);
```

### getAlpha

Retrieve alpha color of a pixel.
Throw GdException if error.

```php
$alpha = $gd->getAlpha($x, $y);
```

### withAlpha

Return an instance with provided alpha color of a pixel.
Throw GdException if error.

```php
$gd = $gd->withAlpha($x, $y, $value);
```

### getRed

Retrieve red color of a pixel.
Throw GdException if error.

```php
$red = $gd->getRed($x, $y);
```

### withRed

Return an instance with provided red color of a pixel.
Throw GdException if error.

```php
$gd = $gd->withRed($x, $y, $value);
```

### getGreen

Retrieve green color of a pixel.
Throw GdException if error.

```php
$green = $gd->getGreen($x, $y);
```

### withGreen

Return an instance with provided green color of a pixel.
Throw GdException if error.

```php
$gd = $gd->withGreen($x, $y, $value);
```

### getBlue

Retrieve blue color of a pixel.
Throw GdException if error.

```php
$blue = $gd->getBlue($x, $y);
```

### withBlue

Return an instance with provided blue color of a pixel.
Throw GdException if error.

```php
$gd = $gd->withBlue($x, $y, $value);
```
