# Graphics Draw

PHP is not limited to creating just HTML output.
It can also be used to create and manipulate image files in a variety of different image formats,
including GIF, PNG, JPEG, WBMP, and XPM.
Even more conveniently, PHP can output image streams directly to a browser.
You will need to compile PHP with the GD library of image functions for this to work.
GD and PHP may also require other libraries, depending on which image formats you want to work with.

You can use the image functions in PHP to get the size of JPEG, GIF, PNG, SWF, TIFF and JPEG2000 images.

```php
$config_from_resource = [
    "image" => $image_resource
];

$gd_from_resource = new \Samy\Image\Gd($config_from_resource);


$config_from_location = [
    "location" => $image_location
];

$gd_from_location = new \Samy\Image\Gd($config_from_location);


$config_from_scratch = [
    "type" => GdImageType::WEBP,
    "width" => 250,
    "height" => 250
];

$gd_from_scratch = new \Samy\Image\Gd($config_from_scratch);
```

---

## Configuration

### Graphics Draw Configuration

| Name     |  Type   |      Default      | Description                                        |
| -------- | :-----: | :---------------: | -------------------------------------------------- |
| image    | GdImage |       null        | Attach to resource if not empty.                   |
| location | string  |                   | Load resource from image location if not empty.    |
| type     |   int   | GdImageType::WEBP | Provide image type if create image from scratch.   |
| width    |   int   |        250        | Provide image width if create image from scratch.  |
| height   |   int   |        250        | Provide image height if create image from scratch. |

---

## Image Constants

### Image Type

| Name              | Value |
| ----------------- | :---: |
| GdImageType::JPEG |   2   |
| GdImageType::PNG  |   3   |
| GdImageType::BMP  |   6   |
| GdImageType::WBMP |  15   |
| GdImageType::XBM  |  16   |
| GdImageType::WEBP |  18   |

### Arc Draw Style

| Name               | Value |
| :----------------- | :---: |
| GdArcStyle::PIE    |   0   |
| GdArcStyle::CHORD  |   1   |
| GdArcStyle::NOFILL |   2   |
| GdArcStyle::EDGED  |   4   |

### Blur Filter

| Name              | Value |
| :---------------- | :---: |
| GdBlur::GAUSSIAN  |   0   |
| GdBlur::SELECTIVE |   1   |

### Flip Transform

| Name               | Value |
| :----------------- | :---: |
| GdFlip::HORIZONTAL |   1   |
| GdFlip::VERTICAL   |   2   |
| GdFlip::BOTH       |   3   |

### Corner Side Transform

| Name                       | Value |
| :------------------------- | :---: |
| GdCornerSide::TOP_LEFT     |   1   |
| GdCornerSide::TOP_RIGHT    |   2   |
| GdCornerSide::TOP          |   3   |
| GdCornerSide::BOTTOM_RIGHT |   4   |
| GdCornerSide::RIGHT        |   6   |
| GdCornerSide::BOTTOM_LEFT  |   8   |
| GdCornerSide::LEFT         |   9   |
| GdCornerSide::BOTTOM       |  12   |
| GdCornerSide::ALL          |  15   |

### Corner Style Transform

| Name                 | Value |
| :------------------- | :---: |
| GdCornerStyle::FLAT  |   0   |
| GdCornerStyle::ROUND |   1   |
