# Image

---

## Image Interface

Describes Image interface.

### hasImage

Check if instance has a resource image.

```php
$has_image = $gd->hasImage();
```

### getImage

Retrieve resource image.
Throw GdException if error.

```php
$resource = $gd->getImage();
```

### detach

Separates any underlying resource from the instance.
After the instance has been detached, the instance is in an unusable state.
Throw GdException if error.

```php
$resource = $gd->detach();
```

### getType

Retrieve image type.
Throw GdException if error.

```php
$type = $gd->getType();
```

### withType

Return an instance with the provided image type.
Throw GdException if error.

```php
$gd = $gd->withType($type);
```

### getWidth

Retrieve image width.
Throw GdException if error.

```php
$width = $gd->getWidth();
```

### getHeight

Retrieve image height.
Throw GdException if error.

```php
$height = $gd->getHeight();
```

### getMimeType

Retrieve image mime content-type.
Throw GdException if error.

```php
$mimetype = $gd->getMimeType();
```

### getExtension

Retrieve image file extension.
Throw GdException if error.

```php
$extension = $gd->getExtension();
```

### getContent

Retrieve image file content.
Throw GdException if error.

```php
$content = $gd->getContent();
```

### getSize

Retrieve image file size.
Throw GdException if error.

```php
$size = $gd->getSize();
```

### getMd5

Retrieve image md5 hash.
Throw GdException if error.

```php
$md5 = $gd->getMd5();
```

### getSha1

Retrieve image sha1 hash.
Throw GdException if error.

```php
$sha1 = $gd->getSha1();
```

### save

Save image file.
Throw GdException if error.

```php
$gd = $gd->save($filename);
```
