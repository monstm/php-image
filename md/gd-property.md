# Property

---

## Property Interface

Describes Property interface.

### getBlendMode

Retrieve image blend mode.
Throw GdException if error.

```php
$blend_mode = $gd->getBlendMode();
```

### withBlendMode

Return an instance with provided image blend mode.
Throw GdException if error.

```php
$gd = $gd->withBlendMode($blend_mode);
```

### getThickness

Retrieve the thickness of line draw.
Throw GdException if error.

```php
$thickness = $gd->getThickness();
```

### withThickness

Return an instance with provided thickness of line draw.
Throw GdException if error.

```php
$gd = $gd->withThickness($thickness);
```

### getStyle

Retrieve the style of line draw.
Throw GdException if error.

```php
$style = $gd->getStyle();
```

### withStyle

Return an instance with provided style of line draw.
Throw GdException if error.

```php
$gd = $gd->withStyle($style);
```

### hasBrush

Check if instance has a brush image.

```php
$has_brush = $gd->hasBrush();
```

### withBrush

Return an instance with provided brush image.
Throw GdException if error.

```php
$gd = $gd->withBrush($location);
```
