# PHP Image

[
	![](https://badgen.net/packagist/v/samy/image/latest)
	![](https://badgen.net/packagist/license/samy/image)
	![](https://badgen.net/packagist/dt/samy/image)
	![](https://badgen.net/packagist/favers/samy/image)
](https://packagist.org/packages/samy/image)

Image Processing and Generation.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/image
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-image>
* User Manual: <https://monstm.gitlab.io/php-image/>
* Documentation: <https://monstm.alwaysdata.net/php-image/>
* Issues: <https://gitlab.com/monstm/php-image/-/issues>
