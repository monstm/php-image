# Filter

---

## Filter Interface

Describes Filter interface.

### reverseColor

Reverses all colors of the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100
];

$gd = $gd->reverseColor($selection);
```

### grayscale

Converts the image into grayscale.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100
];

$gd = $gd->grayscale($selection);
```

### edgeDetect

Uses edge detection to highlight the edges in the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100
];

$gd = $gd->edgeDetect($selection);
```

### embosses

Embosses the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100
];

$gd = $gd->embosses($selection);
```

### blur

Blurs the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$blur = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "method" => GdBlur::GAUSSIAN
];

$gd = $gd->blur($blur);
```

### sketchy

Uses mean removal to achieve a "sketchy" effect.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$selection = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100
];

$gd = $gd->sketchy($selection);
```

### brightness

Changes the brightness of the image.
Throw GdException if error.
Throw ValidationException if invalid.
Level: -255 ~ 255

```php
$brightness = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "level" => 100
];

$gd = $gd->brightness($brightness);
```

### contrast

Changes the contrast of the image.
Throw GdException if error.
Throw ValidationException if invalid.
Level: -100 ~ 100

```php
$contrast = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "level" => 50
];

$gd = $gd->contrast($contrast);
```

### smooth

Makes the image smoother.
Throw GdException if error.
Throw ValidationException if invalid.
Level: -8 ~ 8

```php
$smooth = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "level" => 4
];

$gd = $gd->smooth($smooth);
```

### pixelate

Applies pixelation effect to the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$pixelate = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "block" => 8,
    "advance" => false
];

$gd = $gd->pixelate($pixelate);
```

### scatter

Applies scatter effect to the image.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$scatter = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "substraction" => 8,
    "addition" => 4,
    "colors" => [ ... ]
];

$gd = $gd->scatter($scatter);
```

### colorize

Converts the image into specify color.
Throw GdException if error.
Throw ValidationException if invalid.

```php
$colorize = [
    "x" => 45,
    "y" => 60,
    "width" => 150,
    "height" => 100,
    "alpha" => 8,
    "red" => 16,
    "green" => 32,
    "blue" => 64
];

$gd = $gd->colorize($colorize);
```
