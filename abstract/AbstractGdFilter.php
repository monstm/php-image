<?php

namespace Samy\Image\Abstract;

use Samy\Image\GdException;
use Samy\Image\Constant\GdBlur;
use Samy\Image\Interface\GdFilterInterface;
use Samy\Validation\ValidationException;

/**
 * This is a simple GD Filter implementation that other GD Filter can inherit from.
 */
abstract class AbstractGdFilter extends AbstractGdTtf implements GdFilterInterface
{
    /**
     * Reverses all colors of the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function reverseColor(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        return $this->filter($Selection, IMG_FILTER_NEGATE);
    }

    /**
     * Converts the image into grayscale.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function grayscale(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        return $this->filter($Selection, IMG_FILTER_GRAYSCALE);
    }

    /**
     * Uses edge detection to highlight the edges in the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function edgeDetect(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        return $this->filter($Selection, IMG_FILTER_EDGEDETECT);
    }

    /**
     * Embosses the image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function embosses(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        return $this->filter($Selection, IMG_FILTER_EMBOSS);
    }

    /**
     * Blurs the image.
     *
     * @param array<string,int> $Blur The blur filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function blur(array $Blur): self
    {
        $this
            ->guardImage()
            ->validateBlur($Blur);

        switch ($Blur["method"]) {
            case GdBlur::SELECTIVE:
                $method = IMG_FILTER_SELECTIVE_BLUR;
                break;
            default:
                $method = IMG_FILTER_GAUSSIAN_BLUR;
                break;
        }

        return $this->filter($Blur, $method);
    }

    /**
     * Uses mean removal to achieve a "sketchy" effect.
     *
     * @param array<string,int> $Selection The filter selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function sketchy(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        return $this->filter($Selection, IMG_FILTER_MEAN_REMOVAL);
    }

    /**
     * Changes the brightness of the image.
     * Level: -255 ~ 255
     *
     * @param array<string,int> $Brightness The brightness filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function brightness(array $Brightness): self
    {
        $this
            ->guardImage()
            ->validateBrightness($Brightness);

        return $this->filter($Brightness, IMG_FILTER_BRIGHTNESS, $Brightness["level"]);
    }

    /**
     * Changes the contrast of the image.
     * Level: -100 ~ 100
     *
     * @param array<string,int> $Contrast The contrast filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function contrast(array $Contrast): self
    {
        $this
            ->guardImage()
            ->validateContrast($Contrast);

        return $this->filter($Contrast, IMG_FILTER_CONTRAST, $Contrast["level"]);
    }

    /**
     * Makes the image smoother.
     * Level: -8 ~ 8
     *
     * @param array<string,int> $Smooth The smooth filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function smooth(array $Smooth): self
    {
        $this
            ->guardImage()
            ->validateSmooth($Smooth);

        return $this->filter($Smooth, IMG_FILTER_SMOOTH, $Smooth["level"]);
    }

    /**
     * Applies pixelation effect to the image.
     *
     * @param array<string,mixed> $Pixelate The pixelate filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function pixelate(array $Pixelate): self
    {
        $this
            ->guardImage()
            ->validatePixelate($Pixelate);

        /** @phpstan-ignore-next-line */
        return $this->filter($Pixelate, IMG_FILTER_PIXELATE, $Pixelate["block"], $Pixelate["advance"]);
    }

    /**
     * Applies scatter effect to the image.
     *
     * @param array<string,mixed> $Scatter The scatter filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function scatter(array $Scatter): self
    {
        $this
            ->guardImage()
            ->validateScatter($Scatter);

        return $this->filter(
            /** @phpstan-ignore-next-line */
            $Scatter,
            IMG_FILTER_SCATTER,
            $Scatter["substraction"],
            $Scatter["addition"],
            $Scatter["colors"] ?? []
        );
    }

    /**
     * Converts the image into specify color.
     *
     * @param array<string,mixed> $Colorize The colorize filter.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function colorize(array $Colorize): self
    {
        $this
            ->guardImage()
            ->validateColorize($Colorize);

        return $this->filter(
            /** @phpstan-ignore-next-line */
            $Colorize,
            IMG_FILTER_COLORIZE,
            $Colorize["red"],
            $Colorize["green"],
            $Colorize["blue"],
            $Colorize["alpha"]
        );
    }

    /**
     * Applies filter image.
     *
     * @param array<string,int> $Selection The filter selection.
     * @param int $Filter Filter mode
     * @param mixed ...$Arguments Filter arguments
     * @return static
     */
    private function filter(array $Selection, int $Filter, mixed ...$Arguments): self
    {
        $image = $this->makeSelection($Selection);
        call_user_func_array("imagefilter", array_merge([$image, $Filter], $Arguments));

        $this->applySelection($Selection, $image);
        imagedestroy($image);

        return $this;
    }
}
