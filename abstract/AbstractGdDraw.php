<?php

namespace Samy\Image\Abstract;

use Samy\Image\GdException;
use Samy\Image\Interface\GdDrawInterface;
use Samy\Validation\ValidationException;

/**
 * This is a simple GD Draw implementation that other GD Draw can inherit from.
 */
abstract class AbstractGdDraw extends AbstractGdPixel implements GdDrawInterface
{
    /**
     * Draw a line.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawLine(array $Area): self
    {
        $this
            ->guardImage()
            ->validateArea($Area);

        $is_styled_color = $this->isStyledColor($Area["color"]);
        $color = ($is_styled_color ? $Area["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Area["color"]),
            $this->greenValue($Area["color"]),
            $this->blueValue($Area["color"]),
            $this->alphaValue($Area["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagerectangle($this->image, $Area["x1"], $Area["y1"], $Area["x2"], $Area["y2"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a rectangle.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawRectangle(array $Area): self
    {
        $this
            ->guardImage()
            ->validateArea($Area);

        $is_styled_color = $this->isStyledColor($Area["color"]);
        $color = ($is_styled_color ? $Area["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Area["color"]),
            $this->greenValue($Area["color"]),
            $this->blueValue($Area["color"]),
            $this->alphaValue($Area["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagerectangle($this->image, $Area["x1"], $Area["y1"], $Area["x2"], $Area["y2"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a filled rectangle.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledRectangle(array $Area): self
    {
        $this
            ->guardImage()
            ->validateArea($Area);

        $is_styled_color = $this->isStyledColor($Area["color"]);
        $color = ($is_styled_color ? $Area["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Area["color"]),
            $this->greenValue($Area["color"]),
            $this->blueValue($Area["color"]),
            $this->alphaValue($Area["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagefilledrectangle($this->image, $Area["x1"], $Area["y1"], $Area["x2"], $Area["y2"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw an ellipse.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawEllipse(array $Area): self
    {
        $this
            ->guardImage()
            ->validateArea($Area)
            ->structureArea($Area);

        $is_styled_color = $this->isStyledColor($Area["color"]);
        $color = ($is_styled_color ? $Area["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Area["color"]),
            $this->greenValue($Area["color"]),
            $this->blueValue($Area["color"]),
            $this->alphaValue($Area["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imageellipse($this->image, $Area["x-center"], $Area["y-center"], $Area["width"], $Area["height"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a filled ellipse.
     *
     * @param array<string,int> $Area The draw area.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledEllipse(array $Area): self
    {
        $this
            ->guardImage()
            ->validateArea($Area)
            ->structureArea($Area);

        $is_styled_color = $this->isStyledColor($Area["color"]);
        $color = ($is_styled_color ? $Area["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Area["color"]),
            $this->greenValue($Area["color"]),
            $this->blueValue($Area["color"]),
            $this->alphaValue($Area["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagefilledellipse($this->image, $Area["x-center"], $Area["y-center"], $Area["width"], $Area["height"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw an arc.
     *
     * @param array<string,int> $Arc The draw arc.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawArc(array $Arc): self
    {
        $this
            ->guardImage()
            ->validateArc($Arc, false)
            ->structureArea($Arc);

        $is_styled_color = $this->isStyledColor($Arc["color"]);
        $color = ($is_styled_color ? $Arc["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Arc["color"]),
            $this->greenValue($Arc["color"]),
            $this->blueValue($Arc["color"]),
            $this->alphaValue($Arc["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagearc(
            $this->image,
            $Arc["x-center"],
            $Arc["y-center"],
            $Arc["width"],
            $Arc["height"],
            $Arc["start-angle"],
            $Arc["end-angle"],
            $color
        );

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a filled arc.
     *
     * @param array<string,int> $Arc The draw arc.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledArc(array $Arc): self
    {
        $this
            ->guardImage()
            ->validateArc($Arc, true)
            ->structureArea($Arc);

        $is_styled_color = $this->isStyledColor($Arc["color"]);
        $color = ($is_styled_color ? $Arc["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Arc["color"]),
            $this->greenValue($Arc["color"]),
            $this->blueValue($Arc["color"]),
            $this->alphaValue($Arc["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagefilledarc(
            $this->image,
            $Arc["x-center"],
            $Arc["y-center"],
            $Arc["width"],
            $Arc["height"],
            $Arc["start-angle"],
            $Arc["end-angle"],
            $color,
            $Arc["style"]
        );

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawPolygon(array $Polygon): self
    {
        $this
            ->guardImage()
            ->validatePolygon($Polygon)
            ->structurePolygon($Polygon);

        $is_styled_color = $this->isStyledColor($Polygon["color"]);
        $color = ($is_styled_color ? $Polygon["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Polygon["color"]),
            $this->greenValue($Polygon["color"]),
            $this->blueValue($Polygon["color"]),
            $this->alphaValue($Polygon["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagepolygon($this->image, $Polygon["_points"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw a filled polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawFilledPolygon(array $Polygon): self
    {
        $this
            ->guardImage()
            ->validatePolygon($Polygon)
            ->structurePolygon($Polygon);

        $is_styled_color = $this->isStyledColor($Polygon["color"]);
        $color = ($is_styled_color ? $Polygon["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Polygon["color"]),
            $this->greenValue($Polygon["color"]),
            $this->blueValue($Polygon["color"]),
            $this->alphaValue($Polygon["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imagefilledpolygon($this->image, $Polygon["_points"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Draw an open polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function drawOpenPolygon(array $Polygon): self
    {
        $this
            ->guardImage()
            ->validatePolygon($Polygon)
            ->structurePolygon($Polygon);

        $is_styled_color = $this->isStyledColor($Polygon["color"]);
        $color = ($is_styled_color ? $Polygon["color"] : imagecolorallocatealpha(
            $this->image,
            $this->redValue($Polygon["color"]),
            $this->greenValue($Polygon["color"]),
            $this->blueValue($Polygon["color"]),
            $this->alphaValue($Polygon["color"])
        ));

        if (!is_int($color)) {
            return $this;
        }

        imageopenpolygon($this->image, $Polygon["_points"], $color);

        if (!$is_styled_color) {
            imagecolordeallocate($this->image, $color);
        }

        return $this->updateSignal();
    }

    /**
     * Return an instance with calculation draw area.
     *
     * @param array<string,int> &$Area The draw area.
     * @return static
     */
    protected function structureArea(array &$Area): self
    {
        $x_min = min($Area["x1"], $Area["x2"]);
        $x_max = max($Area["x1"], $Area["x2"]);
        $y_min = min($Area["y1"], $Area["y2"]);
        $y_max = max($Area["y1"], $Area["y2"]);

        $Area["width"] = $x_max - $x_min;
        $Area["x-center"] = intval(($Area["width"] / 2) + $x_min);

        $Area["height"] = $y_max - $y_min;
        $Area["y-center"] = intval(($Area["height"] / 2) + $y_min);

        return $this;
    }

    /**
     * Return an instance with calculation draw polygon.
     *
     * @param array<string,mixed> &$Polygon The draw polygon.
     * @return static
     */
    private function structurePolygon(array &$Polygon): self
    {
        $Polygon["_points"] = [];
        if (is_array($Polygon["points"])) {
            foreach ($Polygon["points"] as $point) {
                array_push($Polygon["_points"], $point["x"], $point["y"]);
            }
        }

        return $this;
    }

    /**
     * Check if color is build-in styled color.
     *
     * @param int $Color The color value.
     * @return bool
     */
    private function isStyledColor(int $Color): bool
    {
        return in_array($Color, [IMG_COLOR_STYLED, IMG_COLOR_STYLEDBRUSHED]);
    }
}
