<?php

namespace Samy\Image\Abstract;

use GdImage;
use Samy\Image\GdException;
use Samy\Image\Interface\GdPropertyInterface;

/**
 * This is a simple GD Property implementation that other GD Property can inherit from.
 */
abstract class AbstractGdProperty extends AbstractGdImage implements GdPropertyInterface
{
    /** @var bool */
    private $blend_mode = false;

    /** @var int */
    private $thickness = 1;

    /** @var array<int> */
    private $style = [];

    /** @var GdImage */ /** @phpstan-ignore-next-line */
    private $brush = null;

    public function __destruct()
    {
        $this->releaseBrush();

        parent::__destruct();
    }

    /**
     * Release brush image.
     *
     * @return static
     */
    private function releaseBrush(): self
    {
        if ($this->hasBrush()) {
            imagedestroy($this->brush);
            $this->brush = null;
        }

        return $this;
    }

    /**
     * Retrieve image blend mode.
     *
     * @throws GdException If error.
     * @return bool
     */
    public function getBlendMode(): bool
    {
        $this->guardImage();

        return $this->blend_mode;
    }

    /**
     * Return an instance with provided image blend mode.
     *
     * @param bool $BlendMode The blend mode.
     * @throws GdException If error.
     * @return static
     */
    public function withBlendMode(bool $BlendMode): self
    {
        $this->guardImage();

        $this->blend_mode = $BlendMode;
        imagealphablending($this->image, $BlendMode);

        return $this;
    }

    /**
     * Retrieve the thickness of line draw.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getThickness(): int
    {
        $this->guardImage();

        return $this->thickness;
    }

    /**
     * Return an instance with provided thickness of line draw.
     *
     * @param int $Thickness The thickness.
     * @throws GdException If error.
     * @return static
     */
    public function withThickness(int $Thickness): self
    {
        $this->guardImage();

        $this->thickness = $Thickness;
        imagesetthickness($this->image, $Thickness);

        return $this;
    }

    /**
     * Retrieve the style of line draw.
     *
     * @throws GdException If error.
     * @return array<int>
     */
    public function getStyle(): array
    {
        $this->guardImage();

        return $this->style;
    }

    /**
     * Return an instance with provided style of line draw.
     *
     * @param array<int> $Style The style.
     * @throws GdException If error.
     * @return static
     */
    public function withStyle(array $Style): self
    {
        $this->guardImage();

        $this->style = $Style;
        if (!empty($this->style)) {
            imagesetstyle($this->image, $this->style);
        }

        return $this;
    }

    /**
     * Check if instance has a brush image.
     *
     * @return bool
     */
    public function hasBrush(): bool
    {
        return ($this->brush instanceof GdImage);
    }

    /**
     * Return an instance with provided brush image.
     *
     * @param string $Location The brush location.
     * @throws GdException If error.
     * @return static
     */
    public function withBrush(string $Location): self
    {
        $this->guardImage();

        if ($this->hasBrush()) {
            imagedestroy($this->brush);
        }

        $this->brush = $this->loadImageLocation($Location);
        imagesetbrush($this->image, $this->brush);

        return $this;
    }

    /**
     * Return an instance with reset property.
     *
     * @param GdImage $Image The resource image.
     * @throws GdException If error.
     * @return static
     */
    protected function resetProperty(GdImage $Image): self
    {
        $this->guardImage();

        imagedestroy($this->image);
        $this->image = $Image;

        imagealphablending($this->image, $this->blend_mode);
        imagesetthickness($this->image, $this->thickness);

        if (!empty($this->style)) {
            imagesetstyle($this->image, $this->style);
        }

        if ($this->hasBrush()) {
            imagesetbrush($this->image, $this->brush);
        }

        return $this;
    }
}
