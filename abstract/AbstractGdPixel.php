<?php

namespace Samy\Image\Abstract;

use Samy\Image\GdException;
use Samy\Image\Interface\GdPixelInterface;

/**
 * This is a simple GD Pixel implementation that other GD Pixel can inherit from.
 */
abstract class AbstractGdPixel extends AbstractGdProperty implements GdPixelInterface
{
    /**
     * Retrieve alpha value from argb color.
     *
     * @param int $Argb ARGB value
     * @return int
     */
    protected function alphaValue(int $Argb): int
    {
        return (($Argb >> 24) & 0x7F);
    }

    /**
     * Retrieve red value from argb color.
     *
     * @param int $Argb ARGB value
     * @return int
     */
    protected function redValue(int $Argb): int
    {
        return (($Argb >> 16) & 0xFF);
    }

    /**
     * Retrieve green value from argb color.
     *
     * @param int $Argb ARGB value
     * @return int
     */
    protected function greenValue(int $Argb): int
    {
        return (($Argb >> 8) & 0xFF);
    }

    /**
     * Retrieve blue value from argb color.
     *
     * @param int $Argb ARGB value
     * @return int
     */
    protected function blueValue(int $Argb): int
    {
        return (($Argb >> 0) & 0xFF);
    }

    /**
     * Retrieve argb color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getArgb(int $X, int $Y): int
    {
        $this->guardImage();

        return (imagecolorat($this->image, $X, $Y) & 0x7FFFFFFF);
    }

    /**
     * Return an instance with provided argb color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The argb value
     * @throws GdException If error.
     * @return static
     */
    public function withArgb(int $X, int $Y, int $Value): self
    {
        $this->guardImage();

        $color = imagecolorallocatealpha(
            $this->image,
            $this->redValue($Value),
            $this->greenValue($Value),
            $this->blueValue($Value),
            $this->alphaValue($Value)
        );

        if (!is_int($color)) {
            return $this;
        }

        imagesetpixel($this->image, $X, $Y, $color);
        imagecolordeallocate($this->image, $color);

        return $this->updateSignal();
    }

    /**
     * Retrieve alpha color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getAlpha(int $X, int $Y): int
    {
        return $this->alphaValue($this->getArgb($X, $Y));
    }

    /**
     * Return an instance with provided alpha color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The alpha value
     * @throws GdException If error.
     * @return static
     */
    public function withAlpha(int $X, int $Y, int $Value): self
    {
        $mask = $this->getArgb($X, $Y) & 0x00FFFFFF;

        return $this->withArgb($X, $Y, ($mask | (($Value & 0x7F) << 24)));
    }

    /**
     * Retrieve red color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getRed(int $X, int $Y): int
    {
        return $this->redValue($this->getArgb($X, $Y));
    }

    /**
     * Return an instance with provided red color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The red value
     * @throws GdException If error.
     * @return static
     */
    public function withRed(int $X, int $Y, int $Value): self
    {
        $mask = $this->getArgb($X, $Y) & 0x7F00FFFF;

        return $this->withArgb($X, $Y, ($mask | (($Value & 0x7F) << 16)));
    }

    /**
     * Retrieve green color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getGreen(int $X, int $Y): int
    {
        return $this->greenValue($this->getArgb($X, $Y));
    }

    /**
     * Return an instance with provided green color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The green value
     * @throws GdException If error.
     * @return static
     */
    public function withGreen(int $X, int $Y, int $Value): self
    {
        $mask = $this->getArgb($X, $Y) & 0x7FFF00FF;

        return $this->withArgb($X, $Y, ($mask | (($Value & 0x7F) << 8)));
    }

    /**
     * Retrieve blue color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @throws GdException If error.
     * @return int
     */
    public function getBlue(int $X, int $Y): int
    {
        return $this->blueValue($this->getArgb($X, $Y));
    }

    /**
     * Return an instance with provided blue color of a pixel.
     *
     * @param int $X The X coordinate of point
     * @param int $Y The Y coordinate of point
     * @param int $Value The blue value
     * @throws GdException If error.
     * @return static
     */
    public function withBlue(int $X, int $Y, int $Value): self
    {
        $mask = $this->getArgb($X, $Y) & 0x7FFFFF00;

        return $this->withArgb($X, $Y, ($mask | (($Value & 0x7F) << 0)));
    }
}
