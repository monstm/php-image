<?php

namespace Samy\Image\Abstract;

use Samy\Image\Validation\DrawArcValidation;
use Samy\Image\Validation\DrawAreaValidation;
use Samy\Image\Validation\DrawPolygonPointValidation;
use Samy\Image\Validation\DrawPolygonValidation;
use Samy\Image\Validation\FilterBlurValidation;
use Samy\Image\Validation\FilterBrightnessValidation;
use Samy\Image\Validation\FilterColorizeValidation;
use Samy\Image\Validation\FilterContrastValidation;
use Samy\Image\Validation\FilterPixelateValidation;
use Samy\Image\Validation\FilterScatterValidation;
use Samy\Image\Validation\FilterSmoothValidation;
use Samy\Image\Validation\SelectionValidation;
use Samy\Image\Validation\TransformCopyValidation;
use Samy\Image\Validation\TransformCornerValidation;
use Samy\Image\Validation\TtfOptionValidation;
use Samy\Image\Validation\TtfValidation;
use Samy\Validation\ValidationException;

/**
 * This is a simple GD Validation implementation that other GD Validation can inherit from.
 */
abstract class AbstractGdValidation extends AbstractTemporary
{
    /**
     * Return an instance with valid selection.
     *
     * @param array<string,int> $Selection The selection.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateSelection(array $Selection): self
    {
        $validation = new SelectionValidation();
        $validation->validate($Selection);

        return $this;
    }

    /**
     * Return an instance with valid draw area.
     *
     * @param array<string,int> $Area The draw area.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateArea(array $Area): self
    {
        $validation = new DrawAreaValidation();
        $validation->validate($Area);

        return $this;
    }

    /**
     * Return an instance with valid draw arc.
     *
     * @param array<string,int> $Arc The draw arc.
     * @param bool $WithStyle With style key.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateArc(array $Arc, bool $WithStyle): self
    {
        $validation = new DrawArcValidation();
        $validation
            ->withStyle($WithStyle)
            ->validate($Arc);

        return $this;
    }

    /**
     * Return an instance with valid draw polygon.
     *
     * @param array<string,mixed> $Polygon The draw polygon.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validatePolygon(array $Polygon): self
    {
        $validation = new DrawPolygonValidation();
        $validation->validate($Polygon);

        $validation_point = new DrawPolygonPointValidation();

        /** @phpstan-ignore-next-line */
        foreach ($Polygon["points"] as $point) {
            /** @phpstan-ignore-next-line */
            $validation_point->validate($point);
        }

        return $this;
    }

    /**
     * Return an instance with valid TTF.
     *
     * @param array<string,mixed> $Ttf The TTF.
     * @param bool $WithEngrave With engrave key.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateTtf(array $Ttf, bool $WithEngrave): self
    {
        $validation = new TtfValidation();
        $validation
            ->withEngrave($WithEngrave)
            ->validate($Ttf);

        if (is_array($Ttf["option"])) {
            $validation_option = new TtfOptionValidation();
            $validation_option->validate($Ttf["option"]);
        }

        return $this;
    }

    /**
     * Return an instance with valid blur filter.
     *
     * @param array<string,int> $Blur The blur filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateBlur(array $Blur): self
    {
        $validation = new FilterBlurValidation();
        $validation->validate($Blur);

        return $this;
    }

    /**
     * Return an instance with valid brightness filter.
     *
     * @param array<string,int> $Brightness The brightness filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateBrightness(array $Brightness): self
    {
        $validation = new FilterBrightnessValidation();
        $validation->validate($Brightness);

        return $this;
    }

    /**
     * Return an instance with valid contrast filter.
     *
     * @param array<string,int> $Contrast The contrast filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateContrast(array $Contrast): self
    {
        $validation = new FilterContrastValidation();
        $validation->validate($Contrast);

        return $this;
    }

    /**
     * Return an instance with valid smooth filter.
     *
     * @param array<string,int> $Smooth The smooth filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateSmooth(array $Smooth): self
    {
        $validation = new FilterSmoothValidation();
        $validation->validate($Smooth);

        return $this;
    }

    /**
     * Return an instance with valid pixelate filter.
     *
     * @param array<string,mixed> $Pixelate The pixelate filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validatePixelate(array $Pixelate): self
    {
        $validation = new FilterPixelateValidation();
        $validation->validate($Pixelate);

        return $this;
    }

    /**
     * Return an instance with valid scatetr filter.
     *
     * @param array<string,mixed> $Scatter The scatetr filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateScatter(array $Scatter): self
    {
        $validation = new FilterScatterValidation();
        $validation->validate($Scatter);

        return $this;
    }

    /**
     * Return an instance with valid colorize filter.
     *
     * @param array<string,mixed> $Colorize The colorize filter.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateColorize(array $Colorize): self
    {
        $validation = new FilterColorizeValidation();
        $validation->validate($Colorize);

        return $this;
    }

    /**
     * Return an instance with valid copy transform.
     *
     * @param array<string,mixed> $Copy The copy transform.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateCopy(array $Copy): self
    {
        $validation = new TransformCopyValidation();
        $validation->validate($Copy);

        return $this;
    }

    /**
     * Return an instance with valid corner transform.
     *
     * @param array<string,int> $Corner The corner transform.
     * @throws ValidationException If invalid.
     * @return static
     */
    protected function validateCorner(array $Corner): self
    {
        $validation = new TransformCornerValidation();
        $validation->validate($Corner);

        return $this;
    }
}
