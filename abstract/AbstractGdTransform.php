<?php

namespace Samy\Image\Abstract;

use GdImage;
use Samy\Image\GdException;
use Samy\Image\Constant\GdAlpha;
use Samy\Image\Constant\GdCornerSide;
use Samy\Image\Constant\GdCornerStyle;
use Samy\Image\Interface\GdTransformInterface;
use Samy\Validation\ValidationException;

/**
 * This is a simple GD Transform implementation that other GD Transform can inherit from.
 */
abstract class AbstractGdTransform extends AbstractGdFilter implements GdTransformInterface
{
    /**
     * Flip image with a given mode.
     *
     * @param int $Mode The flip mode.
     * @throws GdException If error.
     * @return static
     */
    public function flip(int $Mode): self
    {
        $this->guardImage();

        imageflip($this->image, $Mode);

        return $this->updateSignal();
    }

    /**
     * Rotate image with a given angle.
     *
     * @param float $Angle The rotate angle.
     * @throws GdException If error.
     * @return static
     */
    public function rotate(float $Angle): self
    {
        $this->guardImage();

        $transparent = imagecolorallocatealpha($this->image, 0, 0, 0, 127);

        if (!is_int($transparent)) {
            return $this;
        }

        $result = imagerotate($this->image, $Angle, $transparent);
        imagecolordeallocate($this->image, $transparent);

        if (!$result instanceof GdImage) {
            throw new GdException("Failed to rotate image: " . $Angle);
        }

        return $this
            ->resetProperty($result)
            ->updateSignal();
    }

    /**
     * Copy source image.
     *
     * @param array<string,mixed> $Copy The copy transform.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function copy(array $Copy): self
    {
        $this
            ->guardImage()
            ->validateCopy($Copy);

        $image = $this->loadImageLocation(strval($Copy["location"]));

        imagecopyresampled(
            $this->image,
            $image,
            intval($Copy["destination-x"]),
            intval($Copy["destination-y"]),
            intval($Copy["source-x"]),
            intval($Copy["source-y"]),
            intval($Copy["destination-width"]),
            intval($Copy["destination-height"]),
            intval($Copy["source-width"]),
            intval($Copy["source-height"])
        );

        imagedestroy($image);

        return $this->updateSignal();
    }

    /**
     * Crop image.
     *
     * @param array<string,int> $Selection The transform selection.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function crop(array $Selection): self
    {
        $this
            ->guardImage()
            ->validateSelection($Selection);

        $result = imagecrop($this->image, $Selection);
        if (!$result instanceof GdImage) {
            throw new GdException("Failed to crop image: " . json_encode($Selection));
        }

        return $this
            ->resetProperty($result)
            ->updateSignal();
    }

    /**
     * Resize stretch image.
     *
     * @param int $Width The resize width.
     * @param int $Height The resize height.
     * @throws GdException If error.
     * @return static
     */
    public function resize(int $Width, int $Height): self
    {
        $this->guardImage();

        $result = imagescale($this->image, $Width, $Height);
        if (!$result instanceof GdImage) {
            throw new GdException("Failed to resize image: " . $Width . "," . $Height);
        }

        return $this
            ->resetProperty($result)
            ->updateSignal();
    }

    /**
     * Fill thumbnail image.
     *
     * @param int $Width The thumbnail width.
     * @param int $Height The thumbnail height.
     * @throws GdException If error.
     * @return static
     */
    public function thumbnail(int $Width, int $Height): self
    {
        $this->guardImage();

        $image_width = $this->getWidth();
        $image_height = $this->getHeight();

        $crop_ratio = min(
            ($image_width / $Width),
            ($image_height / $Height)
        );

        $crop_width = $Width * $crop_ratio;
        $crop_height = $Height * $crop_ratio;

        $this->crop([
            "x" => intval(($image_width - $crop_width) / 2),
            "y" => intval(($image_height - $crop_height) / 2),
            "width" => intval($crop_width),
            "height" => intval($crop_height)
        ]);

        $this->resize($Width, $Height);

        return $this;
    }

    /**
     * Transform corner.
     *
     * @param array<string,int> $Corner The corner transform.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function corner(array $Corner): self
    {
        $this
            ->guardImage()
            ->validateCorner($Corner);

        switch ($Corner["style"]) {
            case GdCornerStyle::ROUND:
                $points = $this->getCornerRoundPoints($Corner["radius"], $Corner["side"]);
                break;
            default:
                $points = $this->getCornerFlatPoints($Corner["radius"], $Corner["side"]);
                break;
        }

        $blend_mode = $this->getBlendMode();
        $this->withBlendMode(false);
        foreach ($points as $point) {
            $this->withAlpha($point["x"], $point["y"], GdAlpha::TRANSPARENT);
        }

        return $this
            ->withBlendMode($blend_mode)
            ->updateSignal();
    }

    /**
     * Retrieve flat corner transparent pixel points.
     *
     * @param int $Radius The corner radius.
     * @param int $CornerSide The corner side.
     * @return array<array<string,int>>
     */
    private function getCornerFlatPoints(int $Radius, int $CornerSide): array
    {
        $ret = [];
        $width = $this->getWidth();
        $height = $this->getHeight();

        for ($x = 0; $x < $Radius; $x++) {
            $range = $Radius - $x;

            for ($y = 0; $y < $range; $y++) {
                $this->getCornerResultPoints($ret, $x, $y, $width, $height, $CornerSide);
            }
        }

        return $ret;
    }

    /**
     * Retrieve round corner transparent pixel points.
     *
     * @param int $Radius The corner radius.
     * @param int $CornerSide The corner side.
     * @return array<array<string,int>>
     */
    private function getCornerRoundPoints(int $Radius, int $CornerSide): array
    {
        $ret = [];
        $width = $this->getWidth();
        $height = $this->getHeight();

        for ($x = 0; $x < $Radius; $x++) {
            $range = $Radius - $x;

            for ($y = 0; $y < $range; $y++) {
                $a = $Radius - $x;
                $b = $Radius - $y;
                $c = sqrt(pow($a, 2) + pow($b, 2));

                if ($c > $Radius) {
                    $this->getCornerResultPoints($ret, $x, $y, $width, $height, $CornerSide);
                }
            }
        }

        return $ret;
    }

    /**
     * Retrieve flat corner transparent pixel points.
     *
     * @param array<string,int> $Result The corner radius.
     * @param int $X The x point.
     * @param int $Y The y point.
     * @param int $Width The image width.
     * @param int $Height The image height.
     * @param int $CornerSide The corner side.
     * @return void
     */
    private function getCornerResultPoints(
        array &$Result,
        int $X,
        int $Y,
        int $Width,
        int $Height,
        int $CornerSide
    ): void {
        $x_inverse = $Width - ($X + 1);
        $y_inverse = $Height - ($Y + 1);

        if (($CornerSide & GdCornerSide::TOP_LEFT) == GdCornerSide::TOP_LEFT) {
            array_push($Result, ["x" => $X, "y" => $Y]);
        }

        if (($CornerSide & GdCornerSide::TOP_RIGHT) == GdCornerSide::TOP_RIGHT) {
            array_push($Result, ["x" => $x_inverse, "y" => $Y]);
        }

        if (($CornerSide & GdCornerSide::BOTTOM_RIGHT) == GdCornerSide::BOTTOM_RIGHT) {
            array_push($Result, ["x" => $x_inverse, "y" => $y_inverse]);
        }

        if (($CornerSide & GdCornerSide::BOTTOM_LEFT) == GdCornerSide::BOTTOM_LEFT) {
            array_push($Result, ["x" => $X, "y" => $y_inverse]);
        }
    }
}
