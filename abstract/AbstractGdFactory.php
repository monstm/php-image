<?php

namespace Samy\Image\Abstract;

use GdImage;
use Samy\Image\GdException;
use Samy\Image\Constant\GdImageType;

/**
 * This is a simple GD Factory implementation that other GD Factory can inherit from.
 */
abstract class AbstractGdFactory extends AbstractGdValidation
{
    private const MAP = [
        ["type" => GdImageType::BMP, "load" => "imagecreatefrombmp", "save" => "imagebmp"],
        //~ ["type" => GdImageType::GIF, "load" => "imagecreatefromgif", "save" => "imagegif"],
        ["type" => GdImageType::JPEG, "load" => "imagecreatefromjpeg", "save" => "imagejpeg"],
        ["type" => GdImageType::PNG, "load" => "imagecreatefrompng", "save" => "imagepng"],
        ["type" => GdImageType::WBMP, "load" => "imagecreatefromwbmp", "save" => "imagewbmp"],
        ["type" => GdImageType::WEBP, "load" => "imagecreatefromwebp", "save" => "imagewebp"],
        ["type" => GdImageType::XBM, "load" => "imagecreatefromxbm", "save" => "imagexbm"]
    ];

    /**
     * Retrieve image map.
     *
     * @param int $Type The image type.
     * @throws GdException If error.
     * @return array<string,string>
     */
    protected function getMap(int $Type): array
    {
        $load = "";
        $save = "";
        $supported = false;
        foreach (self::MAP as $map) {
            if (($Type == $map["type"]) && function_exists($map["load"]) && function_exists($map["save"])) {
                $load = $map["load"];
                $save = $map["save"];
                $supported = true;
                break;
            }
        }

        if (!$supported) {
            throw new GdException("Unsupported image type: " . $Type);
        }

        return [
            "load" => $load,
            "save" => $save
        ];
    }

    /**
     * Create blank image.
     *
     * @param int $Width The image width.
     * @param int $Height The image height.
     * @throws GdException If error.
     * @return GdImage
     */
    protected function createImage(int $Width, int $Height): GdImage
    {
        $ret = imagecreatetruecolor($Width, $Height);
        if (!$ret instanceof GdImage) {
            throw new GdException("Cannot initialize new GD image stream");
        }

        return $ret;
    }

    /**
     * Load image location.
     *
     * @param string $Location The image location.
     * @throws GdException If error.
     * @return GdImage
     */
    protected function loadImageLocation(string $Location): GdImage
    {
        // $filename = "";
        // if (is_file($Location)) {
        //     $filename = $Location;
            // }elseif(filter_var($Location, FILTER_VALIDATE_URL)){
                // download image from url
                // $filename = download location
        // }

        return $this->loadImageFile($Location);
    }

    /**
     * Load image file.
     *
     * @param string $Filename The image filename.
     * @throws GdException If error.
     * @return GdImage
     */
    private function loadImageFile(string $Filename): GdImage
    {
        if (!is_file($Filename)) {
            throw new GdException("'" . $Filename . "' is not exists");
        }

        $type = exif_imagetype($Filename);
        if (!is_int($type)) {
            throw new GdException("'" . $Filename . "' is not an image");
        }

        $map = $this->getMap($type);

        $ret = null;
        if (is_callable($map["load"])) {
            $ret = @call_user_func_array($map["load"], [$Filename]);
        }

        if (!$ret instanceof GdImage) {
            throw new GdException("Failed to load image file: " . $Filename);
        }

        return $ret;
    }
}
