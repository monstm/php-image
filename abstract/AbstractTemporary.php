<?php

namespace Samy\Image\Abstract;

/**
 * This is a simple Temporary implementation that other Temporary can inherit from.
 */
abstract class AbstractTemporary
{
    /** @var array<string> */
    private $temporary = [];

    public function __destruct()
    {
        $this->clearTemporary();
    }

    /**
     * Return an instance with the added temprary filename.
     *
     * @param string $Filename The temporary filename.
     * @return static
     */
    protected function addTemporary(string $Filename): self
    {
        if (!in_array($Filename, $this->temporary)) {
            array_push($this->temporary, $Filename);
        }

        return $this;
    }

    /**
     * Clear temporary data.
     *
     * @return static
     */
    protected function clearTemporary(): self
    {
        foreach ($this->temporary as $temporary) {
            if (is_file($temporary)) {
                @unlink($temporary);
            }
        }

        $this->temporary = [];

        return $this;
    }

    /**
     * Retrieve a random temporary filename.
     *
     * @param string $Namespace The file namespace.
     * @param string $Extension The file extension.
     * @return string
     */
    protected function getRandomFilename(string $Namespace, string $Extension = "tmp"): string
    {
        $directory = sys_get_temp_dir();
        $filename = implode("-", ["image", $Namespace, sha1(strval(time()) . strval(rand()))]);

        return $directory . DIRECTORY_SEPARATOR . $filename . "." . $Extension;
    }
}
