<?php

namespace Samy\Image\Abstract;

use GdImage;
use Samy\Image\GdException;
use Samy\Image\Interface\GdImageInterface;

/**
 * This is a simple GD Image implementation that other GD Image can inherit from.
 */
abstract class AbstractGdImage extends AbstractGdFactory implements GdImageInterface
{
    /** @var int */
    private $type = 0;

    /** @var string */
    private $filename = "";

    /** @var bool */
    private $updated = false;

    /** @var GdImage */ /** @phpstan-ignore-next-line */
    protected $image = null;

    public function __destruct()
    {
        $this->releaseImage();

        parent::__destruct();
    }

    /**
     * Release resource image.
     *
     * @return static
     */
    private function releaseImage(): self
    {
        if ($this->hasImage()) {
            imagedestroy($this->image);
            $this->image = null;
        }

        return $this;
    }

    /**
     * Guard resource image.
     *
     * @throws GdException If error.
     * @return static
     */
    protected function guardImage(): self
    {
        if (!$this->image instanceof GdImage) {
            throw new GdException("Empty resource image");
        }

        return $this;
    }

    /**
     * Guard image file.
     *
     * @throws GdException If error.
     * @return static
     */
    protected function guardFile(): self
    {
        $this->guardImage();

        if ($this->updated) {
            $extension = $this->getExtension();
            $this->filename = $this->getRandomFilename("gd", $extension);

            $this
                ->addTemporary($this->filename)
                ->save($this->filename);

            $this->updated = false;
        }

        if (!is_file($this->filename)) {
            throw new GdException("Empty file image");
        }

        return $this;
    }

    /**
     * Send update signal.
     *
     * @return static
     */
    protected function updateSignal(): self
    {
        $this->updated = true;

        return $this;
    }

    /**
     * Make image selection.
     *
     * @param array<string,int> $Selection The image selection.
     * @return GdImage
     */
    protected function makeSelection(array $Selection): GdImage
    {
        $ret = $this->createImage($Selection["width"], $Selection["height"]);

        imagecopy(
            $ret,
            $this->image,
            0,
            0,
            $Selection["x"],
            $Selection["y"],
            $Selection["width"],
            $Selection["height"]
        );

        return $ret;
    }

    /**
     * Apply image selection.
     *
     * @param array<string,int> $Selection The image selection.
     * @param GdImage $Image The source image.
     * @return static
     */
    protected function applySelection(array $Selection, GdImage $Image): self
    {
        imagecopy(
            $this->image,
            $Image,
            $Selection["x"],
            $Selection["y"],
            0,
            0,
            $Selection["width"],
            $Selection["height"]
        );

        return $this->updateSignal();
    }

    /**
     * Check if instance has a resource image.
     *
     * @return bool
     */
    public function hasImage(): bool
    {
        return ($this->image instanceof GdImage);
    }

    /**
     * Retrieve resource image.
     *
     * @throws GdException If error.
     * @return GdImage
     */
    public function getImage(): GdImage
    {
        $this->guardImage();

        return $this->image;
    }

    /**
     * Separates any underlying resource from the instance.
     * After the instance has been detached, the instance is in an unusable state.
     *
     * @throws GdException If error.
     * @return GdImage
     */
    public function detach(): GdImage
    {
        $ret = $this->getImage();
        $this->image = null;

        return $ret;
    }

    /**
     * Retrieve image type.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getType(): int
    {
        $this->guardImage();

        return $this->type;
    }

    /**
     * Return an instance with the provided image type.
     *
     * @param int $Type The image type.
     * @throws GdException If error.
     * @return static
     */
    public function withType(int $Type): self
    {
        $this->guardImage();
        $this->getMap($Type);

        $this->type = $Type;

        return $this->updateSignal();
    }

    /**
     * Retrieve image width.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getWidth(): int
    {
        $this->guardImage();

        return imagesx($this->image);
    }

    /**
     * Retrieve image height.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getHeight(): int
    {
        $this->guardImage();

        return imagesy($this->image);
    }

    /**
     * Retrieve image mime content-type.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getMimeType(): string
    {
        $this->guardImage();

        return image_type_to_mime_type($this->type);
    }

    /**
     * Retrieve image file extension.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getExtension(): string
    {
        $this->guardImage();
        $ret = image_type_to_extension($this->type, false);

        return is_string($ret) ? $ret : "";
    }

    /**
     * Retrieve image file content.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getContent(): string
    {
        $this->guardFile();
        $ret = file_get_contents($this->filename);

        return is_string($ret) ? $ret : "";
    }

    /**
     * Retrieve image file size.
     *
     * @throws GdException If error.
     * @return int
     */
    public function getSize(): int
    {
        $this->guardFile();
        $ret = filesize($this->filename);

        return is_int($ret) ? $ret : 0;
    }

    /**
     * Retrieve image md5 hash.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getMd5(): string
    {
        $this->guardFile();
        $ret = md5_file($this->filename);

        return is_string($ret) ? $ret : "";
    }

    /**
     * Retrieve image sha1 hash.
     *
     * @throws GdException If error.
     * @return string
     */
    public function getSha1(): string
    {
        $this->guardFile();
        $ret = sha1_file($this->filename);

        return is_string($ret) ? $ret : "";
    }

    /**
     * Save image file.
     *
     * @param string $Filename The filename.
     * @throws GdException If error.
     * @return static
     */
    public function save(string $Filename): self
    {
        $this->guardImage();

        $map = $this->getMap($this->type);
        if (is_callable($map["save"])) {
            call_user_func_array($map["save"], [$this->image, $Filename]);
        }

        return $this;
    }
}
