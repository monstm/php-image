<?php

namespace Samy\Image\Abstract;

use Samy\Image\GdException;
use Samy\Image\Interface\GdTtfInterface;
use Samy\Validation\ValidationException;

/**
 * This is a simple GD TrueType Fonts implementation that other GD TrueType Fonts can inherit from.
 */
abstract class AbstractGdTtf extends AbstractGdDraw implements GdTtfInterface
{
    private const DEFAULT_SIZE = 12;
    private const DEFAULT_ANGLE = 0;
    private const DEFAULT_OPTION = null;

    /**
     * Retrieve the bounding box of a text using TrueType fonts.
     *
     * @param array<string,mixed> $Ttf The ttf configuration.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return array<string,int>
     */
    public function ttfBox(array $Ttf): array
    {
        $this
            ->guardImage()
            ->validateTtf($Ttf, false)
            ->structureTtf($Ttf);

        $box = imageftbbox($Ttf["size"], $Ttf["angle"], $Ttf["font"], $Ttf["text"], $Ttf["option"]);

        $x_bottom_left = $box[0] ?? 0;  // lower left corner, X position
        $y_bottom_left = $box[1] ?? 0;  // lower left corner, Y position
        $x_bottom_right = $box[2] ?? 0; // lower right corner, X position
        $y_bottom_right = $box[3] ?? 0; // lower right corner, Y position
        $x_top_right = $box[4] ?? 0;    // upper right corner, X position
        $y_top_right = $box[5] ?? 0;    // upper right corner, Y position
        $x_top_left = $box[6] ?? 0;     // upper left corner, X position
        $y_top_left = $box[7] ?? 0;     // upper left corner, Y position

        $area = [
            "x1" => min($x_bottom_left, $x_bottom_right, $x_top_right, $x_top_left),    // lowest X position
            "y1" => min($y_bottom_left, $y_bottom_right, $y_top_right, $y_top_left),    // lowest Y position
            "x2" => max($x_bottom_left, $x_bottom_right, $x_top_right, $x_top_left),    // highest X position
            "y2" => max($y_bottom_left, $y_bottom_right, $y_top_right, $y_top_left)     // highest Y position
        ];

        $this->structureArea($area);

        return [
            "x1" => $area["x1"],
            "y1" => $area["y1"],
            "x2" => $area["x2"],
            "y2" => $area["y2"],
            "width" => $area["width"],
            "height" => $area["height"],
            "x-center" => $area["x-center"],
            "y-center" => $area["y-center"],
            "x-bottom-left" => $x_bottom_left,
            "y-bottom-left" => $y_bottom_left,
            "x-bottom-right" => $x_bottom_right,
            "y-bottom-right" => $y_bottom_right,
            "x-top-right" => $x_top_right,
            "y-top-right" => $y_top_right,
            "x-top-left" => $x_top_left,
            "y-top-left" => $y_top_left
        ];
    }

    /**
     * Return an instance with the provided TrueType fonts text.
     *
     * @param array<string,mixed> $Ttf The ttf configuration.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     * @return static
     */
    public function ttfText(array $Ttf): self
    {
        $this
            ->guardImage()
            ->validateTtf($Ttf, true)
            ->structureTtf($Ttf);

        $color = imagecolorallocatealpha(
            $this->image,
            $this->redValue($Ttf["color"]),
            $this->greenValue($Ttf["color"]),
            $this->blueValue($Ttf["color"]),
            $this->alphaValue($Ttf["color"])
        );

        if (!is_int($color)) {
            return $this;
        }

        imagettftext(
            $this->image,
            $Ttf["size"],
            $Ttf["angle"],
            $Ttf["x"],
            $Ttf["y"],
            $color,
            $Ttf["font"],
            $Ttf["text"],
            $Ttf["option"]
        );

        imagecolordeallocate($this->image, $color);

        return $this->updateSignal();
    }

    /**
     * Return an instance with structure TTF.
     *
     * @param array<string,mixed> &$Ttf The TTF.
     * @return static
     */
    private function structureTtf(array &$Ttf): self
    {
        $structure = [
            "size" => self::DEFAULT_SIZE,
            "angle" => self::DEFAULT_ANGLE,
            "option" => self::DEFAULT_OPTION
        ];

        foreach ($structure as $key => $value) {
            if (!array_key_exists($key, $Ttf)) {
                $Ttf[$key] = $value;
            }
        }

        return $this;
    }
}
