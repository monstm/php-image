<?php

namespace Samy\Image\Abstract;

/**
 * This is a simple GD implementation that other GD can inherit from.
 */
abstract class AbstractGd extends AbstractGdTransform
{
}
