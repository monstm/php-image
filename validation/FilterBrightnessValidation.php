<?php

namespace Samy\Image\Validation;

class FilterBrightnessValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("level", ["required" => true, "type" => "integer", "min" => -255, "max" => 255]);
    }
}
