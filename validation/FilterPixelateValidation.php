<?php

namespace Samy\Image\Validation;

class FilterPixelateValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("block", ["required" => true, "type" => "integer"])
            ->withRule("advance", ["type" => "boolean|null"]);
    }
}
