<?php

namespace Samy\Image\Validation;

class FilterColorizeValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("alpha", ["required" => true, "type" => "integer", "min" => 0, "max" => 127])
            ->withRule("red", ["required" => true, "type" => "integer", "min" => 0, "max" => 255])
            ->withRule("green", ["required" => true, "type" => "integer", "min" => 0, "max" => 255])
            ->withRule("blue", ["required" => true, "type" => "integer", "min" => 0, "max" => 255]);
    }
}
