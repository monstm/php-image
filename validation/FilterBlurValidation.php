<?php

namespace Samy\Image\Validation;

use Samy\Image\Constant\GdBlur;

class FilterBlurValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule(
                "method",
                ["required" => true, "type" => "integer", "in" => [GdBlur::GAUSSIAN, GdBlur::SELECTIVE]]
            );
    }
}
