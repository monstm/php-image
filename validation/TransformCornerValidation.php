<?php

namespace Samy\Image\Validation;

use Samy\Image\Constant\GdCornerStyle;
use Samy\Validation\Validation;

class TransformCornerValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("radius", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("side", ["required" => true, "type" => "integer"])
            ->withRule("style", [
                "required" => true, "type" => "integer", "in" => [GdCornerStyle::FLAT, GdCornerStyle::ROUND]
            ]);
    }
}
