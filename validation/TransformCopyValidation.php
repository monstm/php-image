<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class TransformCopyValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("location", ["required" => true, "type" => "string"])
            ->withRule("source-x", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("source-y", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("source-width", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("source-height", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("destination-x", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("destination-y", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("destination-width", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("destination-height", ["required" => true, "type" => "integer", "min" => 0]);
    }
}
