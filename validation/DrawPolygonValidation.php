<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class DrawPolygonValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("color", ["required" => true, "type" => "integer"])
            ->withRule("points", ["required" => true, "type" => "array"]);
    }
}
