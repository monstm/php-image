<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class DrawArcValidation extends Validation
{
    private const STYLE_RULE = ["required" => true, "type" => "integer", "min" => 0];

    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("x1", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("y1", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("x2", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("y2", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("color", ["required" => true, "type" => "integer"])
            ->withRule("start-angle", ["required" => true, "type" => "integer"])
            ->withRule("end-angle", ["required" => true, "type" => "integer"]);
    }

    public function withStyle(bool $Value): self
    {
        if ($Value) {
            $this->withRule("style", self::STYLE_RULE);
        } else {
            $this->withoutRule("style");
        }

        return $this;
    }
}
