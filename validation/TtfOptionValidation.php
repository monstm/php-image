<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class TtfOptionValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("linespacing", ["type" => "integer|double", "min" => 0]);
    }
}
