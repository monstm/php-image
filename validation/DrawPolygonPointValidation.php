<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class DrawPolygonPointValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("x", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("y", ["required" => true, "type" => "integer", "min" => 0]);
    }
}
