<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class TtfValidation extends Validation
{
    private const X_RULE = ["required" => true, "type" => "integer", "min" => 0];
    private const Y_RULE = ["required" => true, "type" => "integer", "min" => 0];
    private const COLOR_RULE = ["required" => true, "type" => "integer"];

    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("font", ["required" => true, "type" => "string"])
            ->withRule("text", ["required" => true, "type" => "string"])
            ->withRule("size", ["type" => "integer|double"])
            ->withRule("angle", ["type" => "integer|double"])
            ->withRule("option", ["type" => "array|null"]);
    }

    public function withEngrave(bool $Value): self
    {
        if ($Value) {
            $this
                ->withRule("x", self::X_RULE)
                ->withRule("y", self::Y_RULE)
                ->withRule("color", self::COLOR_RULE);
        } else {
            $this
                ->withoutRule("x")
                ->withoutRule("y")
                ->withoutRule("color");
        }

        return $this;
    }
}
