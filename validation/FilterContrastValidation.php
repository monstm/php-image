<?php

namespace Samy\Image\Validation;

class FilterContrastValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("level", ["required" => true, "type" => "integer", "min" => -100, "max" => 100]);
    }
}
