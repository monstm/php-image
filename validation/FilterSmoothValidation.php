<?php

namespace Samy\Image\Validation;

class FilterSmoothValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("level", ["required" => true, "type" => "integer", "min" => -8, "max" => 8]);
    }
}
