<?php

namespace Samy\Image\Validation;

class FilterScatterValidation extends SelectionValidation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("substraction", ["required" => true, "type" => "integer"])
            ->withRule("addition", ["required" => true, "type" => "integer"])
            ->withRule("colors", ["type" => "array|null"]);
    }
}
