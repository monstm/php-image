<?php

namespace Samy\Image\Validation;

use Samy\Validation\Validation;

class DrawAreaValidation extends Validation
{
    public function __construct()
    {
        parent::__construct();

        $this
            ->withRule("x1", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("y1", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("x2", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("y2", ["required" => true, "type" => "integer", "min" => 0])
            ->withRule("color", ["required" => true, "type" => "integer"]);
    }
}
