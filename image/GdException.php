<?php

namespace Samy\Image;

use Exception;

/**
 * Every GD exception MUST implement this interface.
 */
class GdException extends Exception
{
}
