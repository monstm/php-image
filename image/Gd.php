<?php

namespace Samy\Image;

use GdImage;
use Samy\Image\Abstract\AbstractGd;
use Samy\Image\Constant\GdImageType;
use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

/**
 * Simple Gd implementation.
 */
class Gd extends AbstractGd
{
    /**
     * @param array<string,mixed> $Config The image configuration.
     * @throws GdException If error.
     * @throws ValidationException If invalid.
     */
    public function __construct(array $Config = [])
    {
        $validation = new Validation();
        $validation
            ->withRule("image", ["type" => "object"])
            ->withRule("location", ["type" => "string"])
            ->withRule("type", ["type" => "integer"])
            ->withRule("width", ["type" => "integer", "min" => 0])
            ->withRule("height", ["type" => "integer", "min" => 0])
            ->validate($Config);

        $image = $Config["image"] ?? null;
        $location = strval($Config["location"] ?? "");
        $width = intval($Config["width"] ?? 250);
        $height = intval($Config["height"] ?? 250);
        $type = intval($Config["type"] ?? GdImageType::WEBP);

        if ($image instanceof GdImage) {
            $this->image = $image;
        } elseif (!empty($location)) {
            $this->image = $this->loadImageLocation($location);

            if (is_file($location)) {
                $exif_imagetype = exif_imagetype($location);
                if (is_int($exif_imagetype)) {
                    $type = $exif_imagetype;
                }
            }
        } else {
            $this->image = $this->createImage($width, $height);
        }

        $this->withType($type);
    }
}
