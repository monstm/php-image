<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Corner Side implementation.
 */
class GdCornerSide
{
    public const TOP_LEFT       = 0x1;
    public const TOP_RIGHT      = 0x2;
    public const BOTTOM_RIGHT   = 0x4;
    public const BOTTOM_LEFT    = 0x8;

    public const TOP            = self::TOP_LEFT | self::TOP_RIGHT;
    public const RIGHT          = self::TOP_RIGHT | self::BOTTOM_RIGHT;
    public const LEFT           = self::TOP_LEFT | self::BOTTOM_LEFT;
    public const BOTTOM         = self::BOTTOM_RIGHT | self::BOTTOM_LEFT;

    public const ALL            = self::TOP | self::BOTTOM;
}
