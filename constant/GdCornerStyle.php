<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Corner Style implementation.
 */
class GdCornerStyle
{
    public const FLAT   = 0;
    public const ROUND  = 1;
}
