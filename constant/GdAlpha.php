<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Alpha Channel implementation.
 */
class GdAlpha
{
    public const SOLID          = 0;
    public const TRANSPARENT    = 127;
}
