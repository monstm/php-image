<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Blur Filter implementation.
 */
class GdBlur
{
    public const GAUSSIAN   = 0;
    public const SELECTIVE  = 1;
}
