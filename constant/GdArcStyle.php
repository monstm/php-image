<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Arc Style implementation.
 */
class GdArcStyle
{
    public const PIE    = IMG_ARC_PIE;
    public const CHORD  = IMG_ARC_CHORD;
    public const NOFILL = IMG_ARC_NOFILL;
    public const EDGED  = IMG_ARC_EDGED;
}
