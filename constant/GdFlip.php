<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Flip Filter implementation.
 */
class GdFlip
{
    public const HORIZONTAL = IMG_FLIP_HORIZONTAL;
    public const VERTICAL   = IMG_FLIP_VERTICAL;
    public const BOTH       = IMG_FLIP_BOTH;
}
