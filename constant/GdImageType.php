<?php

namespace Samy\Image\Constant;

/**
 * Simple GD Image Type implementation.
 */
class GdImageType
{
    public const JPEG   = IMAGETYPE_JPEG;
    public const PNG    = IMAGETYPE_PNG;
    public const BMP    = IMAGETYPE_BMP;
    public const WBMP   = IMAGETYPE_WBMP;
    public const XBM    = IMAGETYPE_XBM;
    public const WEBP   = IMAGETYPE_WEBP;
}
