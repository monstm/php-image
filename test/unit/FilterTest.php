<?php

namespace Test\Unit;

use Samy\Image\Gd;

class FilterTest extends AbstractTestCase
{
    /**
     * Test reverse color.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataReverseColor
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testReverseColor(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->reverseColor($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test grayscale.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataGrayscale
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGrayscale(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->grayscale($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test edge detect.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataEdgeDetect
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testEdgeDetect(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->edgeDetect($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test embosses.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataEmbosses
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testEmbosses(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->embosses($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test blur.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataBlur
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testBlur(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->blur($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test sketchy.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataSketchy
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testSketchy(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->sketchy($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test brightness.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataBrightness
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testBrightness(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->brightness($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test contrast.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataContrast
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testContrast(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->contrast($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test smooth.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataSmooth
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testSmooth(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->smooth($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test pixelate.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataPixelate
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testPixelate(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->pixelate($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test scatter.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataScatter
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testScatter(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->scatter($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test colorize.
     *
     * @dataProvider \Test\DataProvider\FilterDataProvider::dataColorize
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testColorize(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->colorize($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }
}
