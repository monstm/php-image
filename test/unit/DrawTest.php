<?php

namespace Test\Unit;

use Samy\Image\Gd;

class DrawTest extends AbstractTestCase
{
    /**
     * Test line.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataLine
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testLine(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawLine($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test rectangle.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataRectangle
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRectangle(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawRectangle($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test filled rectangle.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataFilledRectangle
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFilledRectangle(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawFilledRectangle($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test ellipse.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataEllipse
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testEllipse(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawEllipse($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test filled ellipse.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataFilledEllipse
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFilledEllipse(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawFilledEllipse($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test arc.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataArc
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testArc(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawArc($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test filled arc.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataFilledArc
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFilledArc(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->drawFilledArc($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test polygon.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataPolygon
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testPolygon(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->drawPolygon($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test filled polygon.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataFilledPolygon
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFilledPolygon(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->drawFilledPolygon($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test open polygon.
     *
     * @dataProvider \Test\DataProvider\DrawDataProvider::dataOpenPolygon
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testOpenPolygon(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->drawOpenPolygon($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }
}
