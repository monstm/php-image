<?php

namespace Test\Unit;

use Samy\Image\Gd;

class PixelTest extends AbstractTestCase
{
    /**
     * Test argb.
     *
     * @dataProvider \Test\DataProvider\PixelDataProvider::dataArgb
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testArgb(array $Data): void
    {
        $x = intval($Data["x"] ?? 0);
        $y = intval($Data["y"] ?? 0);
        $write = intval($Data["write"] ?? 0);

        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertSame(intval($Data["read"] ?? 0), $gd->getArgb($x, $y));
        $this->assertInstanceOf(Gd::class, $gd->withArgb($x, $y, $write));
        $this->assertSame($write, $gd->getArgb($x, $y));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test alpha.
     *
     * @dataProvider \Test\DataProvider\PixelDataProvider::dataAlpha
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testAlpha(array $Data): void
    {
        $x = intval($Data["x"] ?? 0);
        $y = intval($Data["y"] ?? 0);
        $write = intval($Data["write"] ?? 0);

        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertSame(intval($Data["read"] ?? 0), $gd->getAlpha($x, $y));
        $this->assertInstanceOf(Gd::class, $gd->withAlpha($x, $y, $write));
        $this->assertSame($write, $gd->getAlpha($x, $y));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test red.
     *
     * @dataProvider \Test\DataProvider\PixelDataProvider::dataRed
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRed(array $Data): void
    {
        $x = intval($Data["x"] ?? 0);
        $y = intval($Data["y"] ?? 0);
        $write = intval($Data["write"] ?? 0);

        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertSame(intval($Data["read"] ?? 0), $gd->getRed($x, $y));
        $this->assertInstanceOf(Gd::class, $gd->withRed($x, $y, $write));
        $this->assertSame($write, $gd->getRed($x, $y));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test green.
     *
     * @dataProvider \Test\DataProvider\PixelDataProvider::dataGreen
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testGreen(array $Data): void
    {
        $x = intval($Data["x"] ?? 0);
        $y = intval($Data["y"] ?? 0);
        $write = intval($Data["write"] ?? 0);

        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertSame(intval($Data["read"] ?? 0), $gd->getGreen($x, $y));
        $this->assertInstanceOf(Gd::class, $gd->withGreen($x, $y, $write));
        $this->assertSame($write, $gd->getGreen($x, $y));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test blue.
     *
     * @dataProvider \Test\DataProvider\PixelDataProvider::dataBlue
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testBlue(array $Data): void
    {
        $x = intval($Data["x"] ?? 0);
        $y = intval($Data["y"] ?? 0);
        $write = intval($Data["write"] ?? 0);

        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertSame(intval($Data["read"] ?? 0), $gd->getBlue($x, $y));
        $this->assertInstanceOf(Gd::class, $gd->withBlue($x, $y, $write));
        $this->assertSame($write, $gd->getBlue($x, $y));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }
}
