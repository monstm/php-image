<?php

namespace Test\Unit;

use Samy\Image\Gd;
use Samy\Image\GdException;
use Samy\Validation\ValidationException;

class ConstructTest extends AbstractTestCase
{
    /**
     * Test scratch.
     *
     * @dataProvider \Test\DataProvider\ConstructDataProvider::dataScratch
     * @param int $Type
     * @param int $Width
     * @param int $Height
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testScratch(int $Type, int $Width, int $Height, array $Expect): void
    {
        $gd = new Gd([
            "type" => $Type,
            "width" => $Width,
            "height" => $Height
        ]);

        $this->assertConstruct($Expect, $gd);
    }

    /**
     * Test filename.
     *
     * @dataProvider \Test\DataProvider\ConstructDataProvider::dataFilename
     * @param bool $GdException
     * @param string $Filename
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testFilename(bool $GdException, string $Filename, array $Expect): void
    {
        if ($GdException) {
            $this->expectException(GdException::class);
        }

        $gd = new Gd(["location" => $this->getImagePath($Filename)]);

        $this->assertConstruct($Expect, $gd);
    }

    /**
     * Test image.
     *
     * @dataProvider \Test\DataProvider\ConstructDataProvider::dataImage
     * @param bool $ValidationException
     * @param string $Filename
     * @param string $Payload
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testImage(bool $ValidationException, string $Filename, string $Payload, array $Expect): void
    {
        if ($ValidationException) {
            $this->expectException(ValidationException::class);
        }

        $image = null;
        if (is_callable($Payload)) {
            $image = @call_user_func_array($Payload, [$this->getImagePath($Filename)]);
        }

        $gd = new Gd(["image" => $image]);

        $this->assertConstruct($Expect, $gd);
    }

    /**
     * Assert Gd Image Interface.
     *
     * @param array<string,mixed> $Expect
     * @param Gd $Actual
     * @return void
     */
    protected function assertConstruct(array $Expect, Gd $Actual): void
    {
        $this->assertSame($Expect["has-image"] ?? false, $Actual->hasImage());
        $this->assertSame($Expect["type"] ?? 0, $Actual->getType());
        $this->assertSame($Expect["width"] ?? 0, $Actual->getWidth());
        $this->assertSame($Expect["height"] ?? 0, $Actual->getHeight());
        $this->assertSame($Expect["mimetype"] ?? "", $Actual->getMimeType());
        $this->assertSame($Expect["extension"] ?? "", $Actual->getExtension());
        $this->assertSame($Expect["size"] ?? 0, $Actual->getSize());
        $this->assertSame($Expect["md5"] ?? "", $Actual->getMd5());
        $this->assertSame($Expect["sha1"] ?? "", $Actual->getSha1());
    }
}
