<?php

namespace Test\Unit;

use Samy\Image\Gd;

class TtfTest extends AbstractTestCase
{
    /**
     * Test ttf box.
     *
     * @dataProvider \Test\DataProvider\TtfDataProvider::dataBox
     * @param array<string,mixed> $Data
     * @param array<string,mixed> $Expect
     * @return void
     */
    public function testBox(array $Data, array $Expect): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $this->assertSame($Expect, $gd->ttfBox($this->getParsedData($Data)));
    }

    /**
     * Test ttf text.
     *
     * @dataProvider \Test\DataProvider\TtfDataProvider::dataText
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testText(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->ttfText($this->getParsedData($Data)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Retrieve ttf parsed data.
     *
     * @param array<string,mixed> $Data
     * @return array<string,mixed>
     */
    private function getParsedData(array $Data): array
    {
        if (isset($Data["font"])) {
            /** @phpstan-ignore-next-line */
            $Data["font"] = $this->getTtfPath($Data["font"]);
        }

        return $Data;
    }
}
