<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase as PhpUnitAbstractTestCase;

class AbstractTestCase extends PhpUnitAbstractTestCase
{
    /**
     * Retrieve image path.
     *
     * @param string $Filename
     * @return string
     */
    protected function getImagePath(string $Filename): string
    {
        $directory = dirname(__DIR__) . DIRECTORY_SEPARATOR . "image";
        return $directory . DIRECTORY_SEPARATOR . $Filename;
    }

    /**
     * Retrieve ttf path.
     *
     * @param string $Filename
     * @return string
     */
    protected function getTtfPath(string $Filename): string
    {
        $directory = dirname(__DIR__) . DIRECTORY_SEPARATOR . "ttf";
        return $directory . DIRECTORY_SEPARATOR . $Filename;
    }

    /**
     * Retrieve parsed image location.
     *
     * @param array<string,mixed> $Data
     * @return string
     */
    protected function getParsedImage(array $Data): string
    {
        /** @phpstan-ignore-next-line */
        return isset($Data["image"]) ? $this->getImagePath($Data["image"]) : "";
    }

    /**
     * Retrieve parsed sha1.
     *
     * @param array<string,mixed> $Data
     * @return string
     */
    protected function getParsedSha1(array $Data): string
    {
        /** @phpstan-ignore-next-line */
        return $Data["sha1"] ?? "";
    }
}
