<?php

namespace Test\Unit;

use Samy\Image\Gd;

class TransformTest extends AbstractTestCase
{
    /**
     * Test flip.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataFlip
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFlip(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->flip(intval($Data["mode"] ?? 0)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test rotate.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataRotate
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRotate(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->rotate(floatval($Data["angle"] ?? 0)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test copy.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataCopy
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCopy(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->copy($this->getParsedData($Data)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test crop.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataCrop
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCrop(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->crop($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test resize.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataResize
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testResize(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->resize(intval($Data["width"] ?? 0), intval($Data["height"] ?? 0)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test thumbnail.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataThumbnail
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testThumbnail(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        $this->assertInstanceOf(Gd::class, $gd->thumbnail(intval($Data["width"] ?? 0), intval($Data["height"] ?? 0)));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Test corner.
     *
     * @dataProvider \Test\DataProvider\TransformDataProvider::dataCorner
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCorner(array $Data): void
    {
        $gd = new Gd(["location" => $this->getParsedImage($Data)]);
        $gd->withBlendMode(false);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Gd::class, $gd->corner($Data));
        $this->assertSame($this->getParsedSha1($Data), $gd->getSha1());
    }

    /**
     * Retrieve transform parsed data.
     *
     * @param array<string,mixed> $Data
     * @return array<string,mixed>
     */
    private function getParsedData(array $Data): array
    {
        if (isset($Data["location"])) {
            /** @phpstan-ignore-next-line */
            $Data["location"] = $this->getImagePath($Data["location"]);
        }

        return $Data;
    }
}
