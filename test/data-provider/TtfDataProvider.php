<?php

namespace Test\DataProvider;

class TtfDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve box data provider.
     *
     * @return array<mixed>
     */
    public static function dataBox(): array
    {
        return self::jsonData("ttf", "box");
    }

    /**
     * Retrieve text data provider.
     *
     * @return array<mixed>
     */
    public static function dataText(): array
    {
        return self::jsonData("ttf", "text");
    }
}
