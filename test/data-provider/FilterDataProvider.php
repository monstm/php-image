<?php

namespace Test\DataProvider;

class FilterDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve reverse color data provider.
     *
     * @return array<mixed>
     */
    public static function dataReverseColor(): array
    {
        return self::jsonData("filter", "reverse-color");
    }

    /**
     * Retrieve grayscale data provider.
     *
     * @return array<mixed>
     */
    public static function dataGrayscale(): array
    {
        return self::jsonData("filter", "grayscale");
    }

    /**
     * Retrieve edge detect data provider.
     *
     * @return array<mixed>
     */
    public static function dataEdgeDetect(): array
    {
        return self::jsonData("filter", "edge-detect");
    }

    /**
     * Retrieve embosses data provider.
     *
     * @return array<mixed>
     */
    public static function dataEmbosses(): array
    {
        return self::jsonData("filter", "embosses");
    }

    /**
     * Retrieve blur data provider.
     *
     * @return array<mixed>
     */
    public static function dataBlur(): array
    {
        return self::jsonData("filter", "blur");
    }

    /**
     * Retrieve sketchy data provider.
     *
     * @return array<mixed>
     */
    public static function dataSketchy(): array
    {
        return self::jsonData("filter", "sketchy");
    }

    /**
     * Retrieve brightness data provider.
     *
     * @return array<mixed>
     */
    public static function dataBrightness(): array
    {
        return self::jsonData("filter", "brightness");
    }

    /**
     * Retrieve contrast data provider.
     *
     * @return array<mixed>
     */
    public static function dataContrast(): array
    {
        return self::jsonData("filter", "contrast");
    }

    /**
     * Retrieve smooth data provider.
     *
     * @return array<mixed>
     */
    public static function dataSmooth(): array
    {
        return self::jsonData("filter", "smooth");
    }

    /**
     * Retrieve pixelate data provider.
     *
     * @return array<mixed>
     */
    public static function dataPixelate(): array
    {
        return self::jsonData("filter", "pixelate");
    }

    /**
     * Retrieve scatter data provider.
     *
     * @return array<mixed>
     */
    public static function dataScatter(): array
    {
        return self::jsonData("filter", "scatter");
    }

    /**
     * Retrieve colorize data provider.
     *
     * @return array<mixed>
     */
    public static function dataColorize(): array
    {
        return self::jsonData("filter", "colorize");
    }
}
