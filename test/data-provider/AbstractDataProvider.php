<?php

namespace Test\DataProvider;

use Samy\PhpUnit\AbstractDataProvider as PhpUnitAbstractDataProvider;

abstract class AbstractDataProvider extends PhpUnitAbstractDataProvider
{
    /**
     * Retrieve json data.
     *
     * @param string $Name The json name.
     * @return array<mixed>
     */
    public static function json(string $Name): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "json" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".json";

        return parent::json($filename);
    }

    /**
     * Retrieve json data.
     *
     * @param string $Name The json name.
     * @param string $Key
     * @return array<mixed>
     */
    protected static function jsonData(string $Name, string $Key): array
    {
        $json = self::json($Name);
        return is_array($json[$Key]) ? $json[$Key] : [];
    }
}
