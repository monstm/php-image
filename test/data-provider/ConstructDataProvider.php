<?php

namespace Test\DataProvider;

class ConstructDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve scratch data provider.
     *
     * @return array<mixed>
     */
    public static function dataScratch(): array
    {
        return self::json("construct-scratch");
    }

    /**
     * Retrieve filename data provider.
     *
     * @return array<mixed>
     */
    public static function dataFilename(): array
    {
        return self::json("construct-filename");
    }

    /**
     * Retrieve image data provider.
     *
     * @return array<mixed>
     */
    public static function dataImage(): array
    {
        return self::json("construct-image");
    }
}
