<?php

namespace Test\DataProvider;

class TransformDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve flip data provider.
     *
     * @return array<mixed>
     */
    public static function dataFlip(): array
    {
        return self::jsonData("transform", "flip");
    }

    /**
     * Retrieve rotate data provider.
     *
     * @return array<mixed>
     */
    public static function dataRotate(): array
    {
        return self::jsonData("transform", "rotate");
    }

    /**
     * Retrieve copy data provider.
     *
     * @return array<mixed>
     */
    public static function dataCopy(): array
    {
        return self::jsonData("transform", "copy");
    }

    /**
     * Retrieve crop data provider.
     *
     * @return array<mixed>
     */
    public static function dataCrop(): array
    {
        return self::jsonData("transform", "crop");
    }

    /**
     * Retrieve resize data provider.
     *
     * @return array<mixed>
     */
    public static function dataResize(): array
    {
        return self::jsonData("transform", "resize");
    }

    /**
     * Retrieve thumbnail data provider.
     *
     * @return array<mixed>
     */
    public static function dataThumbnail(): array
    {
        return self::jsonData("transform", "thumbnail");
    }

    /**
     * Retrieve corner data provider.
     *
     * @return array<mixed>
     */
    public static function dataCorner(): array
    {
        return self::jsonData("transform", "corner");
    }
}
