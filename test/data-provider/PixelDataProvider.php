<?php

namespace Test\DataProvider;

class PixelDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve argb data provider.
     *
     * @return array<mixed>
     */
    public static function dataArgb(): array
    {
        return self::jsonData("pixel", "argb");
    }

    /**
     * Retrieve alpha data provider.
     *
     * @return array<mixed>
     */
    public static function dataAlpha(): array
    {
        return self::jsonData("pixel", "alpha");
    }

    /**
     * Retrieve red data provider.
     *
     * @return array<mixed>
     */
    public static function dataRed(): array
    {
        return self::jsonData("pixel", "red");
    }

    /**
     * Retrieve green data provider.
     *
     * @return array<mixed>
     */
    public static function dataGreen(): array
    {
        return self::jsonData("pixel", "green");
    }

    /**
     * Retrieve blue data provider.
     *
     * @return array<mixed>
     */
    public static function dataBlue(): array
    {
        return self::jsonData("pixel", "blue");
    }
}
