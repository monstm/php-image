<?php

namespace Test\DataProvider;

class DrawDataProvider extends AbstractDataProvider
{
    /**
     * Retrieve line data provider.
     *
     * @return array<mixed>
     */
    public static function dataLine(): array
    {
        return self::jsonData("draw", "line");
    }

    /**
     * Retrieve rectangle data provider.
     *
     * @return array<mixed>
     */
    public static function dataRectangle(): array
    {
        return self::jsonData("draw", "rectangle");
    }

    /**
     * Retrieve filled rectangle data provider.
     *
     * @return array<mixed>
     */
    public static function dataFilledRectangle(): array
    {
        return self::jsonData("draw", "filled-rectangle");
    }

    /**
     * Retrieve ellipse data provider.
     *
     * @return array<mixed>
     */
    public static function dataEllipse(): array
    {
        return self::jsonData("draw", "ellipse");
    }

    /**
     * Retrieve filled ellipse data provider.
     *
     * @return array<mixed>
     */
    public static function dataFilledEllipse(): array
    {
        return self::jsonData("draw", "filled-ellipse");
    }

    /**
     * Retrieve arc data provider.
     *
     * @return array<mixed>
     */
    public static function dataArc(): array
    {
        return self::jsonData("draw", "arc");
    }

    /**
     * Retrieve filled arc data provider.
     *
     * @return array<mixed>
     */
    public static function dataFilledArc(): array
    {
        return self::jsonData("draw", "filled-arc");
    }

    /**
     * Retrieve polygon data provider.
     *
     * @return array<mixed>
     */
    public static function dataPolygon(): array
    {
        return self::jsonData("draw", "polygon");
    }

    /**
     * Retrieve filled polygon data provider.
     *
     * @return array<mixed>
     */
    public static function dataFilledPolygon(): array
    {
        return self::jsonData("draw", "filled-polygon");
    }

    /**
     * Retrieve open polygon data provider.
     *
     * @return array<mixed>
     */
    public static function dataOpenPolygon(): array
    {
        return self::jsonData("draw", "open-polygon");
    }
}
